# Continuous Integration - CI

Continuous Integration (CI) is a development practice that requires developers to integrate code into a shared repository *frequently*. Each check-in (commit) is then verified in steps such as automatic builds or *tests* - allowing teams to detect problems early. 

*Frequently* - between once a day to dozens of times a day

*Tests* - could be unit/performance/integration tests
