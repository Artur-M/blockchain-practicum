const crypto = require('crypto')

const hash = crypto.createHash('sha256')

hash.update('Tomasz Rozmus')

console.log(hash.digest('hex'))