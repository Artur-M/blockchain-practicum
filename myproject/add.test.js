const add = require('./add')

test('add', () => {
  // arrange
  const a = 1
  const b = 2
  // act
  const result = add(a)
  // assert
  expect(result).toEqual(1)
})

test('add with default values', () => {
  // arrange
  const a = 1
  // act
  const result = add(a)
  // assert
  expect(result).toEqual(1)
})