# NPM project 

## package.json
All npm packages contain a file, usually in the project root, called package.json - this file holds various metadata relevant to the project. This file is used to give information to npm that allows it to identify the project as well as handle the project's dependencies.

https://docs.npmjs.com/files/package.json

## Initing an npm project
`npm init`

## Npm scripts
https://docs.npmjs.com/misc/scripts

`npm start`
