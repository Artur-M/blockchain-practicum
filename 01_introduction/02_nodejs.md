# Introduction to node:
[Basic info](./../stack/nodejs.md)

## Node REPL (Read Eval Print Loop)
Run: `node`
Exit: `.exit`

## Executing a file
`node hello.js`

## Importing build in packages
```
const crypto = require('crypto')`

```

## Node Package Manager (npm)
Package manager for node, npmjs.org, `yarn` is an alternative. https://npmjs.org

## Installing external packages
`$ npm install uuid`
```
const uuid = require('uuid/v1')
console.log(uuid())
```

## Executing a script
`node uuid-example.js`